// @flow

import _ from 'lodash';
import Path from 'path';
import RemoteFS from './remote-fs';
import Semaphore from './semaphore';
import type {Callback, Config, LocalStorage, Logger, StorageList} from '@verdaccio/types';
import type {ILocalData, IPackageStorage} from '@verdaccio/local-storage';

/**
 * Handle local database.
 */
class RemoteDatabase implements ILocalData {

    path: string;
    logger: Logger;
    data: LocalStorage;
    config: Config;
    locked: boolean;
    dbSem: Semaphore;
    s3options: any;
    bucketName: string;
    remotefs: RemoteFS;


    /**
     * Load an parse the local json database.
     */
    constructor(config: Config, logger: Object) {
        this.dbSem = new Semaphore(1);
        const storeConf: any = config.store;
        this.s3options = storeConf.s3storage;
        this.bucketName = storeConf.s3storage.bucket;
        this.config = config;
        this.path = this._buildStoragePath(config);
        this.remotefs = new RemoteFS(this.path, this.bucketName, this.s3options, logger);
        this.logger = logger.logger;
        this.locked = false;
        this._fetchLocalPackages();
    }

    getSecret(): Promise<string> {
        return Promise.resolve(this.data.secret);
    }

    setSecret(secret: string): Promise<any> {
        return new Promise((resolve) => {
            this.data.secret = secret;
            resolve(this._sync());
        });
    }

    /**
     * Add a new element.
     */
    add(name: string, callback: Callback): void {
        if (this.data.list.indexOf(name) === -1) {
            this.data.list.push(name);
            callback(this._sync());
        }
    }

    /**
     * Synchronize the remote storage with the local one.
     * @private
     */
    _sync() {
        if (this.locked) {
            this.logger.info('Database is locked, please check error message printed during startup to prevent data loss.');
            return new Error('Verdaccio database is locked, please contact your administrator to checkout logs during verdaccio startup.');
        }

        this.dbSem.take(1, () => {
            this.remotefs.saveDatabase(this.path, this.data, (err) => {
                this.dbSem.leave(1);
                return err;
            });
        });
    }

    /**
     * Remove an element from the database.
     */
    remove(name: string, callback: Callback): void {
        const pkgName = this.data.list.indexOf(name);
        if (pkgName !== -1) {
            this.data.list.splice(pkgName, 1);
            // Should we have the ability to delete packages?
            this.remotefs.deletePackage(name, callback);
        }
        callback(this._sync());
    }

    /**
     * Return all database elements.
     */
    get(callback: Callback): void {
        callback(null, this.data.list);
    }

    getPackageStorage(packageInfo: string): IPackageStorage {
        // $FlowFixMe
        const packagePath: string = this._getLocalStoragePath(this.config.getMatchedPackagesSpec(packageInfo).storage);

        if (_.isString(packagePath) === false) {
            this.logger.debug({name: packageInfo}, 'this package has no storage defined: @{name}');
            return;
        }

        const packageStoragePath: string = Path.join(packagePath, packageInfo);

        return new RemoteFS(packageStoragePath, this.bucketName, this.s3options, this.logger);
    }

    search(onPackage: verdaccio$Callback, onEnd: verdaccio$Callback, validateName: Function): void {
        // TODO: Implement search
        onEnd();
    }

    /**
     * Verify the right local storage location.
     * @private
     */
    _getLocalStoragePath(path: string): string {
        if (_.isNil(path) === false) {
            return path;
        }

        return this.config.storage;
    }

    /**
     * Build the local database path.
     * @private
     */
    _buildStoragePath(config: Config): string {
        // FUTURE: the database might be parameterizable from config.yaml
        return Path.join(config.storage, '.sinopia-db.json');
    }

    /**
     * Fetch local packages.
     * @private
     */
    _fetchLocalPackages() {
        const database: StorageList = [];
        this.data = {list: database, secret: ''};

        this.remotefs.readDatabase(this.path, (err, dbData) => {
            if (err) {
                this.logger.error(
                    'Failed to read package database file, please check the error printed below:\n' +
                    `File Path: ${this.path}\n\n ${err.message}`,
                    err.stack,
                );
                this.locked = true;
            } else {
                this.data = dbData;
                this.data.secret = this.config.checkSecretKey(this.data.secret);
            }
        });
    }
}

export default RemoteDatabase;
