export default class S3 {
    constructor() {
        this.uploadPart = jest.fn((params, callback) => {
            return callback(null, {});
        });
        this.putObject = jest.fn((params, callback) => {
            return callback(null, params.body);
        });
        this.completeMultipartUpload = jest.fn((params, callback) => {
            return callback(null, {});
        });
        this.createMultipartUpload = jest.fn((params, callback) => {
            return callback(null, {UploadId: 1});
        });
        this.abortMultipartUpload = jest.fn((params, callback) => {
            return callback(null, {});
        });
    }
}
