import Config from './__mocks__/Config';
import S3 from './__mocks__/S3';
import S3WriteStream from '../s3-write-stream';

const config = new Config();
const key = '/test/key';
let s3Client;
let writeStream;

describe('S3WriteStream methods', () => {

    describe('constructor', () => {
        beforeEach(() => {
            s3Client = new S3();
            writeStream = new S3WriteStream(s3Client, config.store.s3storage.bucket, key, {flag: 'flagValue'});
        });

        it('should create a multiPartManager with the given options', (done) => {
            expect(writeStream.multiPartManager).not.toBeNull();
            expect(writeStream.multiPartManager.client).toBe(s3Client);
            expect(writeStream.multiPartManager.key).toBe(key);
            expect(writeStream.multiPartManager.bucket).toBe(config.store.s3storage.bucket);
            expect(writeStream.multiPartManager.options.flag).toBe('flagValue');
            done();
        });

        it('should create a multiPartManager with empty options if no options are passed', (done) => {
            writeStream = new S3WriteStream(s3Client, config.store.s3storage.bucket, key);
            expect(writeStream.multiPartManager).not.toBeNull();
            expect(writeStream.multiPartManager.client).toBe(s3Client);
            expect(writeStream.multiPartManager.key).toBe(key);
            expect(writeStream.multiPartManager.bucket).toBe(config.store.s3storage.bucket);
            expect(Object.keys(writeStream.multiPartManager.options)).toHaveLength(0);
            done();
        });
    });

    describe('write', () => {
        beforeEach(() => {
            s3Client = new S3();
            writeStream = new S3WriteStream(s3Client, config.store.s3storage.bucket, key, {flag: 'flagValue'});
        });

        it('should not create a part if the chunk is smaller than the partBufferSize', (done) => {
            writeStream.write(Buffer.from('First chunk', 'base64'), 'base64', () => {
                expect(writeStream.multiPartManager.parts).toHaveLength(0);
                expect(writeStream.multiPartManager.currentBuffer.equals(Buffer.from('First chunk', 'base64'))).toBe(true);
                done();
            });
        });

        it('should create a part if the chunk is larger than the partBufferSize', (done) => {
            const chunk = Buffer.alloc(5242881, 'a', 'base64');
            writeStream.write(chunk, 'base64', () => {
                expect(writeStream.multiPartManager.parts).toHaveLength(1);
                done();
            });
        });
    });

    describe('end', () => {
        beforeEach(() => {
            s3Client = new S3();
            writeStream = new S3WriteStream(s3Client, config.store.s3storage.bucket, key, {flag: 'flagValue'});
        });

        it('should add a chunk if the chunk is passed', (done) => {
            const chunk = Buffer.alloc(5242881, 'a', 'base64');
            writeStream.end(chunk, 'base64', () => {
                expect(writeStream.multiPartManager.parts).toHaveLength(1);
                done();
            });
        });

        it('should not add a chunk if no chunk is passed', (done) => {
            writeStream.multiPartManager.addChunk = jest.fn(() => {
                return Promise.resolve();
            });

            writeStream.end(null, 'base64', () => {
                expect(writeStream.multiPartManager.currentBuffer).toHaveLength(0);
                done();
            });
        });

        it('should emit a close event', (done) => {
            writeStream.emit = jest.fn();
            writeStream.end(Buffer.alloc(10), 'base64', () => {
                expect(writeStream.emit).toHaveBeenCalledTimes(1);
                expect(writeStream.emit).toHaveBeenCalledWith('close');
                done();
            });
        });

        it('should emit a close event if the buffer is empty', (done) => {
            writeStream.emit = jest.fn();
            writeStream.end(null, 'base64', () => {
                expect(writeStream.emit).toHaveBeenCalledTimes(1);
                expect(writeStream.emit).toHaveBeenCalledWith('close');
                done();
            });
        });

        it('should allow for not sending a callback in case of success', (done) => {
            writeStream.emit = jest.fn();
            expect(() => {
                writeStream.end(null, 'base64');
            }).not.toThrow();
            done();
        });

        it('should flush the currentBuffer before ending the stream', (done) => {
            const chunk = Buffer.alloc(5242881, 'a', 'base64');
            writeStream.write(chunk, 'base64');
            const smallerChunk = Buffer.alloc(100, 'b', 'base64');
            writeStream.write(smallerChunk, 'base64');
            expect(writeStream.multiPartManager.currentBuffer.length).toBeTruthy();
            writeStream.end(null, 'base64', (error) => {
                expect(error).toBeNull();
                expect(writeStream.multiPartManager.currentBuffer.length).toBeFalsy();
                expect(writeStream.multiPartManager.parts).toHaveLength(2);
                done();
            });
        });

        it('should emit an error event if there is an error from creating the multipart upload', (done) => {
            const error = new Error('Create multipart update error');

            s3Client.createMultipartUpload = jest.fn((params, callback) => {
                return callback(error);
            });

            writeStream.on('error', (reason) => {
                expect(reason).toBe(error);
                done();
            });

            const chunk = Buffer.alloc(5242881, 'a', 'base64');
            writeStream.write(chunk, 'base64');
            writeStream.end(Buffer.alloc(10, 'b', 'base64'), 'base64');
        });

        it('should emit an error event in case of an error', (done) => {
            const error = new Error('Some problem');
            writeStream.emit = jest.fn();
            s3Client.completeMultipartUpload = jest.fn((params, callback) => {
                return callback(error);
            });
            const chunk = Buffer.alloc(5242881, 'a', 'base64');
            writeStream.end(chunk, 'base64', (receivedError) => {
                expect(writeStream.emit).toHaveBeenCalledTimes(1);
                expect(writeStream.emit).toHaveBeenCalledWith('error', error);
                expect(receivedError).toBe(error);
                done();
            });
        });

        it('should emit an error event if there are too many parts', (done) => {
            writeStream.on('error', (reason) => {
                expect(reason.message).not.toMatch(/Abort error/);
                expect(reason.message).toMatch(/Unable to create partNumber:[0-9]*/);
                done();
            });

            const chunk = Buffer.alloc(5242881, 'a', 'base64');
            for (let ii = 0; ii < 1001; ii++) {
                writeStream.write(chunk, 'base64', () => {
                });
            }
            writeStream.end(null, 'base64');
        });

        it('should merge the too many parts error with the abort error when it occurs', (done) => {
            const error = new Error('Abort error');
            s3Client.abortMultipartUpload = jest.fn((_, callback) => {
                return callback(error);
            });

            writeStream.on('error', (reason) => {
                expect(reason.message).toMatch(/Abort error/);
                expect(reason.message).toMatch(/Unable to create partNumber:[0-9]*\. The max partNumber is 1000/);
                done();
            });

            const chunk = Buffer.alloc(5242881, 'a', 'base64');
            for (let ii = 0; ii < 1001; ii++) {
                writeStream.write(chunk, 'base64');
            }
            writeStream.end(null, 'base64');
        });

        it('should emit an error event if there is an error during a part upload', (done) => {
            const error = new Error('Upload part error');
            s3Client.uploadPart = jest.fn((_, callback) => {
                return callback(error);
            });

            writeStream.on('error', (reason) => {
                expect(reason).toBe(error);
                done();
            });

            const chunk = Buffer.alloc(5242881, 'a', 'base64');
            writeStream.end(chunk, 'base64');
        });

        it('should merge upload error with abort error if it occurs', (done) => {
            const uploadError = new Error('Upload part error');
            s3Client.uploadPart = jest.fn((_, callback) => {
                return callback(uploadError);
            });
            const abortError = new Error('Abort error');
            s3Client.abortMultipartUpload = jest.fn((_, callback) => {
                return callback(abortError);
            });

            writeStream.on('error', (reason) => {
                expect(reason.message).toMatch(/Upload part error/);
                expect(reason.message).toMatch(/Abort error/);
                done();
            });

            const chunk = Buffer.alloc(5242881, 'a', 'base64');
            writeStream.end(chunk, 'base64');
        });

        it('should emit an error event if there is an error from putObject', (done) => {
            const error = new Error('Put error');
            s3Client.putObject = jest.fn((_, callback) => {
                return callback(error);
            });

            writeStream.on('error', (reason) => {
                expect(reason).toBe(error);
                done();
            });

            const chunk = Buffer.alloc(10, 'a', 'base64');
            writeStream.end(chunk, 'base64');
        });

        it('should call putObject if the chunk is smaller than chunk part', (done) => {
            writeStream.end(Buffer.from('A tiny chunk', 'base64'), 'base64', () => {
                expect(s3Client.putObject).toHaveBeenCalledTimes(1);
                done();
            });

        });

        it('should call the callback with the raised error', (done) => {
            const error = new Error('Put error');
            s3Client.putObject = jest.fn((_, callback) => {
                return callback(error);
            });
            writeStream.on('error', () => {
            });
            const chunk = Buffer.alloc(10, 'a', 'base64');
            writeStream.end(chunk, 'base64', (reason) => {
                expect(reason).toBe(error);
                done();
            });
        });
    });
});
