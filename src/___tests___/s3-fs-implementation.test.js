import S3FSImplementation from '../s3-fs-implementation';
import Logger from './__mocks__/Logger';
import Config from './__mocks__/Config';
import S3WriteStream from '../s3-write-stream';

const logger = new Logger();
const config = new Config();
const s3config = config.store.s3storage;

describe('constructor', () => {
    it('should get the S3 bucket and access credentials from config files', () => {
        const implementation = new S3FSImplementation(s3config, logger);
        expect(implementation.bucketName).toBe(config.store.s3storage.bucket);
        expect(implementation.s3).toBeDefined();
        expect(implementation.logger).toBe(logger);
    });
});

describe('methods', () => {
    let implementation;
    beforeEach(() => {
        implementation = new S3FSImplementation(s3config, logger);
    });

    describe('delete file', () => {
        it('should attempt to delete the file sent at path', (done) => {
            let pathToDelete = '/someFolder/somefile.txt';
            implementation.s3.deleteObject = jest.fn((params, callback) => {
                callback(null, {});
            });
            implementation.deleteFile(pathToDelete, () => {
                expect(implementation.s3.deleteObject).toHaveBeenCalledTimes(1);
                done();
            });
        });

        it('should send the path as the object key to delete', (done) => {
            let pathToDelete = '/some-folder/some-file.txt';
            implementation.s3.deleteObject = jest.fn((params, callback) => {
                expect(params.Key).toBe(pathToDelete);
                callback();
            });
            implementation.deleteFile(pathToDelete, () => {
                done();
            });
        });

        it('should send the bucket name in the params', (done) => {
            let pathToDelete = '/some-folder/some-file.txt';
            implementation.s3.deleteObject = jest.fn((params, callback) => {
                expect(params.Bucket).toBe(config.store.s3storage.bucket);
                callback();
            });
            implementation.deleteFile(pathToDelete, () => {
                done();
            });
        });

        it('should send an error back if the delete operation failed', (done) => {
            let pathToDelete = '/some-folder/some-file.txt';
            implementation.s3.deleteObject = jest.fn((params, callback) => {
                callback(new Error('Some delete error'));
            });
            implementation.deleteFile(pathToDelete, (err) => {
                expect(err.message).toBe('Some delete error');
                done();
            });
        });
    });

    describe('fileExists', () => {
        it('should attempt to get head information for the object', (done) => {
            let path = '/some-folder/some-existing-file.txt';
            implementation.s3.headObject = jest.fn((params, callback) => {
                callback();
            });
            implementation.fileExists(path, () => {
                expect(implementation.s3.headObject).toHaveBeenCalledTimes(1);
                done();
            });
        });

        it('should send the path as the key parameter', (done) => {
            let path = '/some-folder/some-existing-file.txt';
            implementation.s3.headObject = jest.fn((params, callback) => {
                expect(params.Key).toBe(path);
                callback();
            });
            implementation.fileExists(path, () => {
                done();
            });
        });

        it('should send the bucket name in the parameters', (done) => {
            let path = '/some-folder/some-existing-file.txt';
            implementation.s3.headObject = jest.fn((params, callback) => {
                expect(params.Bucket).toBe(config.store.s3storage.bucket);
                callback();
            });
            implementation.fileExists(path, () => {
                done();
            });
        });

        it('should return an error if headObject returns an error', (done) => {
            let path = '/some-folder/some-nonexistent-file.txt';
            const error = new Error('Could not get head data.');
            implementation.s3.headObject = jest.fn((params, callback) => {
                callback(error);
            });
            implementation.fileExists(path, (err) => {
                expect(err).toBe(error);
                done();
            });
        });

        it('should return head data if the file exists and no errors are raised', (done) => {
            let path = '/some-folder/some-existing-image.jpg';
            const headData = {
                AcceptRanges: 'bytes',
                ContentLength: 3191,
                ContentType: 'image/jpeg',
                ETag: '"6805f2cfc46c0f04559748bb039d69ae"',
                LastModified: new Date(),
                Metadata: {},
                VersionId: 'null',
            };
            implementation.s3.headObject = jest.fn((params, callback) => {
                callback(null, headData);
            });
            implementation.fileExists(path, (err, data) => {
                expect(err).toBeNull();
                expect(data).toBe(headData);
                done();
            });
        });
    });

    describe('renameFile', () => {
        it('should attempt to copy the original object to a new destination', (done) => {
            const originPath = '/path-to-origin/file.txt';
            const destinationPath = '/path-to-destination/file.txt';
            implementation.s3.copyObject = jest.fn((_params, callback) => {
                callback(null, {});
            });
            implementation.renameFile(originPath, destinationPath, () => {
                expect(implementation.s3.copyObject).toHaveBeenCalledTimes(1);
                done();
            });
        });

        it('should attempt to remove origin file if the copy is successful', (done) => {
            const originPath = '/path-to-origin/file.txt';
            const destinationPath = '/path-to-destination/file.txt';
            implementation.s3.copyObject = jest.fn((_params, callback) => {
                callback(null, {});
            });
            implementation.s3.deleteObject = jest.fn((_params, callback) => {
                callback(null, {});
            });
            implementation.renameFile(originPath, destinationPath, () => {
                expect(implementation.s3.deleteObject).toHaveBeenCalledTimes(1);
                done();
            });
        });

        it('should set the origin in the copy call to the bucket name and the origin path', (done) => {
            const originPath = '/path-to-origin/file.txt';
            const destinationPath = '/path-to-destination/file.txt';
            implementation.s3.copyObject = jest.fn((params) => {
                expect(params.CopySource).toBe(`/${config.store.s3storage.bucket}/${originPath}`);
                done();
            });
            implementation.renameFile(originPath, destinationPath);
        });

        it('should set the destination in the copy params', (done) => {
            const originPath = '/path-to-origin/file.txt';
            const destinationPath = '/path-to-destination/file.txt';
            implementation.s3.copyObject = jest.fn((params) => {
                expect(params.Key).toBe(destinationPath);
                done();
            });
            implementation.renameFile(originPath, destinationPath);
        });

        it('should set the bucket in the copy params', (done) => {
            const originPath = '/path-to-origin/file.txt';
            const destinationPath = '/path-to-destination/file.txt';
            implementation.s3.copyObject = jest.fn((params) => {
                expect(params.Bucket).toBe(config.store.s3storage.bucket);
                done();
            });
            implementation.renameFile(originPath, destinationPath);
        });

        it('should set the key in the delete params to the origin path', (done) => {
            const originPath = '/path-to-origin/file.txt';
            const destinationPath = '/path-to-destination/file.txt';
            implementation.s3.copyObject = jest.fn((params, callback) => {
                callback(null, {});
            });
            implementation.s3.deleteObject = jest.fn((params) => {
                expect(params.Key).toBe(originPath);
                done();
            });
            implementation.renameFile(originPath, destinationPath);
        });

        it('should set the bucket in the delete params', (done) => {
            const originPath = '/path-to-origin/file.txt';
            const destinationPath = '/path-to-destination/file.txt';
            implementation.s3.copyObject = jest.fn((params, callback) => {
                callback(null, {});
            });
            implementation.s3.deleteObject = jest.fn((params) => {
                expect(params.Bucket).toBe(config.store.s3storage.bucket);
                done();
            });
            implementation.renameFile(originPath, destinationPath);
        });

        it('should not attempt to delete the origin if the copy command did not succeed', (done) => {
            const originPath = '/path-to-origin/file.txt';
            const destinationPath = '/path-to-destination/file.txt';
            implementation.s3.copyObject = jest.fn((params, callback) => {
                callback(new Error('No Such Key').code = 'NoSuchKey');
            });
            implementation.s3.deleteObject = jest.fn(() => {
            });

            implementation.renameFile(originPath, destinationPath, () => {
                expect(implementation.s3.deleteObject).not.toHaveBeenCalled();
                done();
            });
        });

        it('should return the delete error if delete was not successful', (done) => {
            const originPath = '/path-to-origin/file.txt';
            const destinationPath = '/path-to-destination/file.txt';
            const error = new Error('Error deleting file.');
            implementation.s3.copyObject = jest.fn((params, callback) => {
                callback(null, {});
            });
            implementation.s3.deleteObject = jest.fn((params, callback) => {
                callback(error);
            });

            implementation.renameFile(originPath, destinationPath, (callbackErr) => {
                expect(callbackErr).toBe(error);
                done();
            });
        });

        it('should return the results for the copy and the delete operations on success', (done) => {
            const originPath = '/path-to-origin/file.txt';
            const destinationPath = '/path-to-destination/file.txt';
            const copyData = {ETag: 'copytag'};
            const delData = {ETag: 'deleteTag'};
            implementation.s3.copyObject = jest.fn((params, callback) => {
                callback(null, copyData);
            });
            implementation.s3.deleteObject = jest.fn((params, callback) => {
                callback(null, delData);
            });

            implementation.renameFile(originPath, destinationPath, (err, cData, dData) => {
                expect(err).toBeNull();
                expect(cData).toBe(copyData);
                expect(dData).toBe(dData);
                done();
            });
        });
    });

    describe('readFile', () => {
        it('should attempt to get file data for the given path', (done) => {
            let path = '/some-folder/some-existing-file.txt';
            implementation.s3.getObject = jest.fn((params, callback) => {
                callback();
            });
            implementation.readFile(path, () => {
                expect(implementation.s3.getObject).toHaveBeenCalledTimes(1);
                done();
            });
        });

        it('should send the path as the key parameter', (done) => {
            let path = '/some-folder/some-existing-file.txt';
            implementation.s3.getObject = jest.fn((params, callback) => {
                expect(params.Key).toBe(path);
                callback();
            });
            implementation.readFile(path, () => {
                done();
            });
        });

        it('should send the bucket name in the parameters', (done) => {
            let path = '/some-folder/some-existing-file.txt';
            implementation.s3.getObject = jest.fn((params, callback) => {
                expect(params.Bucket).toBe(config.store.s3storage.bucket);
                callback();
            });
            implementation.readFile(path, () => {
                done();
            });
        });

        it('should return an error if headObject returns an error', (done) => {
            let path = '/some-folder/some-nonexistent-file.txt';
            const error = new Error('Could not get head data.');
            implementation.s3.getObject = jest.fn((params, callback) => {
                callback(error);
            });
            implementation.readFile(path, (err) => {
                expect(err).toBe(error);
                done();
            });
        });

        it('should return head data if the file exists and no errors are raised', (done) => {
            let path = '/some-folder/some-existing-text.txt';
            const testData = {
                'test': 'This is a test file.',
            };
            const headData = JSON.stringify(testData);
            implementation.s3.getObject = jest.fn((params, callback) => {
                callback(null, {Body: headData});
            });
            implementation.readFile(path, (err, data) => {
                expect(err).toBeNull();
                expect(data).toEqual(testData);
                done();
            });
        });
    });

    describe('writeFile', () => {
        it('should attempt to get file data for the given path', (done) => {
            let path = '/some-folder/some-existing-file.txt';
            let data = 'Test data.';
            implementation.s3.putObject = jest.fn((params, callback) => {
                callback();
            });
            implementation.writeFile(path, data, () => {
                expect(implementation.s3.putObject).toHaveBeenCalledTimes(1);
                done();
            });
        });

        it('should send the path as the key parameter', (done) => {
            let path = '/some-folder/some-existing-file.txt';
            let data = 'Test data.';
            implementation.s3.putObject = jest.fn((params, callback) => {
                expect(params.Key).toBe(path);
                callback();
            });
            implementation.writeFile(path, data, () => {
                done();
            });
        });

        it('should send the bucket name in the parameters', (done) => {
            let path = '/some-folder/some-existing-file.txt';
            let data = 'Test data.';
            implementation.s3.putObject = jest.fn((params, callback) => {
                expect(params.Bucket).toBe(config.store.s3storage.bucket);
                callback();
            });
            implementation.writeFile(path, data, () => {
                done();
            });
        });

        it('should send the data in the parameters', (done) => {
            let path = '/some-folder/some-existing-file.txt';
            let data = 'Test data.';
            implementation.s3.putObject = jest.fn((params, callback) => {
                expect(params.Body).toBe(data);
                callback();
            });
            implementation.writeFile(path, data, () => {
                done();
            });
        });

        it('should return an error if headObject returns an error', (done) => {
            let path = '/some-folder/some-existing-file.txt';
            let data = 'Test data.';
            const error = new Error('Could not get head data.');
            implementation.s3.putObject = jest.fn((params, callback) => {
                callback(error);
            });
            implementation.writeFile(path, data, (err) => {
                expect(err).toBe(error);
                done();
            });
        });

        it('should return head data if the file exists and no errors are raised', (done) => {
            let path = '/some-folder/some-existing-file.txt';
            let data = 'Test data.';
            let writeData = {
                ETag: '6805f2cfc46c0f04559748bb039d69ae',
                VersionId: 'Ri.vC6qVlA4dEnjgRV4ZHsHoFIjqEMNt',
            };
            implementation.s3.putObject = jest.fn((params, callback) => {
                callback(null, writeData);
            });
            implementation.writeFile(path, data, (err, data) => {
                expect(err).toBeNull();
                expect(data).toBe(writeData);
                done();
            });
        });
    });

    describe('deleteDir', () => {
        it('should call deleteFile with a path ending in / if path does not already end in / ', (done) => {
            implementation.deleteFile = jest.fn((path, callback) => {
                expect(path[path.length - 1]).toEqual('/');
                callback();
            });
            implementation.deleteDir('/some/path', () => {
                expect(implementation.deleteFile).toHaveBeenCalledTimes(1);
                done();
            });
        });

        it('should not add a / if path is already a folder path', (done) => {
            implementation.deleteFile = jest.fn((path, callback) => {
                expect(path[path.length - 1]).toEqual('/');
                expect(path[path.length - 2]).not.toEqual('/');
                callback();
            });
            implementation.deleteDir('/some/path/', () => {
                expect(implementation.deleteFile).toHaveBeenCalledTimes(1);
                done();
            });
        });
    });

    describe('createReadStream', () => {
        it('should call the getObject function to create an AWS request', (done) => {
            const path = '/some-folder/some-existing-file.txt';
            implementation.s3.getObject = jest.fn((params, callback) => {
                expect(callback).toBeUndefined();
                return {
                    createReadStream: jest.fn(() => {
                        return 'test';
                    }),
                };
            });
            implementation.createReadStream(path);
            expect(implementation.s3.getObject).toHaveBeenCalledTimes(1);
            done();
        });

        it('should send the path as the key parameter', (done) => {
            const path = '/some-folder/some-existing-file.txt';
            implementation.s3.getObject = jest.fn((params) => {
                expect(params.Key).toBe(path);
                return {createReadStream: jest.fn()};
            });
            implementation.createReadStream(path);
            done();
        });

        it('should send the bucket name in the parameters', (done) => {
            const path = '/some-folder/some-existing-file.txt';
            implementation.s3.getObject = jest.fn((params) => {
                expect(params.Bucket).toBe(config.store.s3storage.bucket);
                return {createReadStream: jest.fn()};
            });
            implementation.createReadStream(path);
            done();
        });

        it('should call the createReadStream on the returned request', (done) => {
            const path = '/some-folder/some-existing-file.txt';
            const mockRequest = {
                createReadStream: jest.fn(),
            };

            implementation.s3.getObject = jest.fn(() => {
                return mockRequest;
            });
            implementation.createReadStream(path);
            expect(mockRequest.createReadStream).toHaveBeenCalledTimes(1);
            done();
        });

        it('should return the stream created by the createReadStream call', (done) => {
            const path = '/some-folder/some-existing-file.txt';
            const mockReturnValue = 'mockReturnValue';
            const mockRequest = {
                createReadStream: jest.fn(() => {
                    return mockReturnValue;
                }),
            };

            implementation.s3.getObject = jest.fn(() => {
                return mockRequest;
            });
            expect(implementation.createReadStream(path)).toEqual(mockReturnValue);
            done();
        });
    });

    describe('createWriteStream', () => {
        it('should return an instance of a S3WriteStream', (done) => {
            const writeStream = implementation.createWriteStream('/some/path');
            expect(writeStream instanceof S3WriteStream).toBeTruthy();
            done();
        });

        it('should pass the path, bucket name and S3 client to the constructor', (done) => {
            const path = '/some/other/path';
            const writeStream = implementation.createWriteStream(path);
            expect(writeStream.multiPartManager.client).toBe(implementation.s3);
            expect(writeStream.multiPartManager.bucket).toBe(config.store.s3storage.bucket);
            expect(writeStream.multiPartManager.key).toBe(path);
            done();
        });
    });
});
