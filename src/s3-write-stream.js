// @flow
/*
 * The following code was adapted from the s3-fs by Riptide Software Inc.
 */

/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-2016 Riptide Software Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

const Writable: any = require('stream').Writable;

const defaultOptions: any = {};
const maxParts: number = 1000;
const partBufferSize: number = 5242880;

type S3Callback = (error: ?Error, value: ?any) => ?Promise<any>;
type S3Function = (params: any, callback: S3Callback) => Promise<any>;
type S3Client = {
    uploadPart: S3Function,
    putObject: S3Function,
    completeMultipartUpload: S3Function,
    createMultipartUpload: S3Function,
    abortMultipartUpload: S3Function
};

class MultiPartManager {

    client: S3Client;
    bucket: string;
    key: string;
    parts: Array<Promise<any>>;
    partNumber: number;
    currentBuffer: Buffer;
    bytesWritten: number;
    options: any;
    _uploadIdPromise: Promise<number> | null;

    constructor(client: any, bucket: string, key: string, options: any) {
        this.client = client;
        this.bucket = bucket;
        this.key = key;
        this.parts = [];
        this.partNumber = 0;
        this.currentBuffer = Buffer.alloc(0);
        this.bytesWritten = 0;
        this.options = options || {};
        this._uploadIdPromise = null;
    }

    addChunk(chunk) {
        this.currentBuffer = Buffer.concat([this.currentBuffer, chunk]);
        if (this.currentBuffer.length >= partBufferSize) {
            const promise = this.addPart(this.currentBuffer);
            this.parts.push(promise);
            this.currentBuffer = new Buffer(0);
        }
    }

    flush() {
        if (this.currentBuffer.length) {
            const promise = this.addPart(this.currentBuffer);
            this.parts.push(promise);
            this.currentBuffer = new Buffer(0);
        }
    }

    addPart(buffer): Promise<{ PartNumber: string }> {
        const partNumber: number = ++this.partNumber;
        let returnPromise: Promise<{ PartNumber: string }>;

        if (partNumber > maxParts) {
            const err: Error = new Error(`Unable to create partNumber:${partNumber}. The max partNumber is ${maxParts}`);
            returnPromise = this.abort()
                .then(() => {
                    return Promise.reject(err);
                })
                .catch((reason: Error) => {
                    const message: string = err.message;
                    const mergedError: Error = new Error(`Error aborting upload: ${reason.message}. Upload was aborted due to: ${message}`);
                    return Promise.reject(mergedError);
                });
        } else {
            returnPromise = this.uploadId()
                .then((uploadId: number) => {
                    return new Promise((resolve, reject) => {
                        const params: any = {
                            Bucket: this.bucket,
                            Key: this.key,
                            Body: buffer,
                            UploadId: uploadId,
                            PartNumber: partNumber,
                        };

                        this.client.uploadPart(params, (err: ?Error, result: any) => {
                            if (err) {
                                const message: string = err.message;
                                return this.abort()
                                    .then(() => {
                                        reject(err);
                                    })
                                    .catch((reason: Error) => {
                                        const mergedError: Error = new Error(`Error aborting upload: ${reason.message}. Upload was aborted due to: ${message}`);
                                        reject(mergedError);
                                    });
                            }
                            result.PartNumber = partNumber;
                            this.bytesWritten += buffer.length;
                            resolve(result);
                        });
                    });
                });
        }
        // Synchronously attach an empty error handler to suppress unhandled rejection warnings.
        // Error will be handled asynchronously on complete() call.
        returnPromise.catch(() => {
        });
        return returnPromise;
    }

    abort(): Promise<number> {
        return this.uploadId()
            .then((uploadId) => {
                return new Promise((resolve: Function, reject: Function) => {
                    const params: any = {
                        Bucket: this.bucket,
                        Key: this.key,
                        UploadId: uploadId,
                    };

                    this.client.abortMultipartUpload(params, (err: ?Error) => {
                        if (err) {
                            reject(err);
                        } else {
                            resolve();
                        }
                    });
                });
            });
    }

    uploadId(): Promise<number> {
        if (!this._uploadIdPromise) {
            this._uploadIdPromise = new Promise((resolve, reject) => {
                const params = Object.assign({
                    Bucket: this.bucket,
                    Key: this.key,
                }, this.options);

                this.client.createMultipartUpload(params, (err: ?Error, data: any) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(data.UploadId);
                    }
                });
            });
            return this._uploadIdPromise;
        } else {
            return this._uploadIdPromise;
        }
    }

    put(): Promise<any> {
        return new Promise((resolve, reject) => {
            const putParams = Object.assign({
                Bucket: this.bucket,
                Key: this.key,
                Body: this.currentBuffer,
            }, this.options);

            this.client.putObject(putParams, (err, data) => {
                if (err) {
                    reject(err);
                } else {
                    this.bytesWritten += this.currentBuffer.length;
                    resolve(data);
                }
            });
        });
    }

    complete(): Promise<any> {
        if (this.partNumber > 0) {
            return this.uploadId()
                .then((uploadId) => {
                    this.flush();
                    return Promise.all(this.parts)
                        .then((parts: Array<any>) => {
                            return new Promise((resolve, reject) => {
                                const params = {
                                    Bucket: this.bucket,
                                    Key: this.key,
                                    UploadId: uploadId,
                                    MultipartUpload: {
                                        Parts: parts,
                                    },
                                };
                                this.client.completeMultipartUpload(params, (err: ?Error, data: ?any) => {
                                    if (err) {
                                        reject(err);
                                    } else {
                                        resolve(data);
                                    }
                                });
                            });
                        });
                });
        } else {
            // if we did not reach the part limit of 5M just use putObject
            return this.put();
        }
    }
}


class S3WriteStream extends Writable {
    bytesWritten: number;
    multiPartManager: MultiPartManager;

    constructor(client: S3Client, bucket: string, key: string, options: any) {
        const streamOptions: any = Object.assign({}, defaultOptions, options);
        super(streamOptions);
        this.multiPartManager = new MultiPartManager(client, bucket, key, options);
        this.bytesWritten = 0;
        setTimeout(() => {
            this.emit('open');
        }, 1);
    }

    write(chunk: any, encoding?: string, cb?: S3Callback) {
        this.multiPartManager.addChunk(chunk);
        if (cb) {
            cb(null);
        }
    }

    end(chunk?: any, encoding?: string, cb?: S3Callback) {
        if (chunk) {
            this.multiPartManager.addChunk(chunk);
        }
        this.multiPartManager.complete()
            .then(() => {
                this.bytesWritten = this.multiPartManager.bytesWritten;
                this.emit('close');
                if (cb) {
                    cb(null);
                }
            })
            .catch((reason) => {
                this.bytesWritten = this.multiPartManager.bytesWritten;
                this.emit('error', reason);
                if (cb) {
                    return cb(reason);
                }
            });
    }
}

export default S3WriteStream;
