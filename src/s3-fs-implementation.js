// @flow

import S3WriteStream from './s3-write-stream';
import {S3} from 'aws-sdk';
import type {Logger} from '@verdaccio/types';


class S3FSImplementation {

    bucketName: string;
    s3: S3;
    logger: Logger;

    constructor(options: any, logger: Logger) {
        const params: any = {
            accessKeyId: options.accessKeyId,
            secretAccessKey: options.secretAccessKey,
            region: options.region || 'us-east-2',
        };
        this.s3 = new S3(params);
        this.bucketName = options.bucket;
        this.logger = logger;
    }

    deleteFile(path: string, callback: Function) {
        const delParams: any = {
            Bucket: this.bucketName,
            Key: path,
        };
        this.s3.deleteObject(delParams, (err, delData) => {
            if (err) {
                callback(err);
            } else {
                callback(null, delData);
            }
        });
    }

    deleteDir(path: string, callback: Function) {
        let finalPath: string = path;
        if (finalPath[finalPath.length - 1] !== '/') {
            finalPath = finalPath + '/';
        }
        this.deleteFile(finalPath, callback);
    }

    fileExists(path: string, callback: Function) {
        const headParams: any = {
            Bucket: this.bucketName,
            Key: path,
        };
        this.s3.headObject(headParams, (err, headData) => {
            if (err) {
                callback(err);
            } else {
                callback(null, headData);
            }
        });
    }

    renameFile(sourcePath: string, destinationPath: string, callback: Function) {
        const copyParams: any = {
            Bucket: this.bucketName,
            CopySource: `/${this.bucketName}/${sourcePath}`,
            Key: destinationPath,
        };
        this.s3.copyObject(copyParams, (copyErr, copyData) => {
            if (copyErr) {
                callback(copyErr);
            } else {
                const delParams: any = {
                    Bucket: this.bucketName,
                    Key: sourcePath,
                };
                this.s3.deleteObject(delParams, (delErr, delData) => {
                    if (delErr) {
                        callback(delErr);
                    } else {
                        callback(null, copyData, delData);
                    }
                });
            }
        });
    }

    readFile(path: string, callback: Function) {
        const params: any = {
            Bucket: this.bucketName,
            Key: path,
        };
        this.s3.getObject(params, (err, res) => {
            if (err) {
                callback(err);
            } else {
                try {
                    const stringData: string = res.Body.toString('utf8'); // The response body is a byte array.
                    const data: Object = JSON.parse(stringData);
                    return callback(null, data);
                } catch (err) {
                    return callback(err);
                }
            }
        });
    }

    createReadStream(path: string) {
        const params: any = {
            Bucket: this.bucketName,
            Key: path,
        };
        return this.s3.getObject(params).createReadStream();
    }

    createWriteStream(path: string) {
        return new S3WriteStream(this.s3, this.bucketName, path, {});
    }

    writeFile(path: string, data: string, callback: Function) {
        const params: any = {
            Bucket: this.bucketName,
            Key: path,
            Body: data,
        };
        this.s3.putObject(params, (err, data) => {
            if (err) {
                callback(err);
            } else {
                callback(err, data);
            }
        });
    }

}

export default S3FSImplementation;
