// @flow

type TaskType = {
    n: number,
    task: Function,
}

class Semaphore {
    capacity: number;
    current: number;
    queue: Array<TaskType>;
    firstHere: boolean;

    constructor(capacity: number) {
        capacity = capacity || 1;
        this.capacity = capacity;
        this.current = 0;
        this.queue = [];
        this.firstHere = false;
    }

    take(n: number, taskToRun: Function) {
        let isFirst: number = 0;
        if (this.firstHere === false) {
            this.current++;
            this.firstHere = true;
            isFirst = 1;
        }

        n = n || 1;

        const item: TaskType = {
            n: n,
            task: taskToRun,
        };

        let task: Function = item.task;
        item.task = () => {
            task(this.leave.bind(this));
        };

        if (this.current + item.n - isFirst > this.capacity) {
            if (isFirst === 1) {
                this.current--;
                this.firstHere = false;
            }
            return this.queue.push(item);
        }

        this.current += item.n - isFirst;
        item.task(this.leave.bind(this));
        if (isFirst === 1) {
            this.firstHere = false;
        }
    }

    leave(n: number) {
        n = n || 1;

        this.current -= n;

        if (!this.queue.length) {
            if (this.current < 0) {
                throw new Error('leave called too many times.');
            }
            return;
        }

        const item: TaskType = this.queue[0];

        if (item.n + this.current > this.capacity) {
            return;
        }

        this.queue.shift();
        this.current += item.n;

        process.nextTick(item.task);
    }
}

export {Semaphore};

export default Semaphore;
