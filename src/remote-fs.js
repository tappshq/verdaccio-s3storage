// @flow

import path from 'path';
import _ from 'lodash';
import Semaphore from './semaphore';
import S3FSImplementation from './s3-fs-implementation';
import type {HttpError} from 'http-errors';
import createError from 'http-errors';
import type {IUploadTarball} from '@verdaccio/streams';
import {ReadTarball, UploadTarball} from '@verdaccio/streams';
import type {Callback, Logger, Package} from '@verdaccio/types';
import type {ILocalPackageManager} from '@verdaccio/local-storage';

export const fileExist: string = 'EEXISTS';
export const noSuchFile: string = 'ENOENT';

const resourceNotAvailable: string = 'EAGAIN';
const pkgFileName = 'package.json';
const fSError = function(message: string, code: number = 409): HttpError {
    const err: HttpError = createError(code, message);
    // $FlowFixMe
    err.code = message;
    return err;
};

const ErrorCode = {
    get503: () => {
        return fSError('resource temporarily unavailable', 500);
    },
    get404: (customMessage) => {
        return fSError('no such package available', 404);
    },
};

class RemoteFS implements ILocalPackageManager {

    path: string;
    logger: Logger;
    remoteFSImpl: S3FSImplementation;
    packageSemaphores: {};

    constructor(path: string, bucketName: string, remoteFSOptions: any, logger: Logger) {
        this.path = path;
        this.logger = logger;
        this.remoteFSImpl = new S3FSImplementation(remoteFSOptions, logger);
        this.packageSemaphores = {};
    }

    /**
     *  This function allows to update the package thread-safely
     Algorithm:
     1. lock package.json for writing
     2. read package.json
     3. updateFn(pkg, cb), and wait for cb
     4. write package.json.tmp
     5. move package.json.tmp package.json
     6. callback(err?)
     * @param {*} name
     * @param {*} updateHandler
     * @param {*} onWrite
     * @param {*} transformPackage
     * @param {*} onEnd
     */
    updatePackage(name: string,
                  updateHandler: Callback,
                  onWrite: Callback,
                  transformPackage: Function,
                  onEnd: Callback) {
        const path = this._getStorage(pkgFileName);
        this._lockAndReadJSON(path, (err, json) => {
            const self = this;
            // callback that cleans up lock first
            const unLockCallback = function unLockCallback(err, ...args) {
                self._unlockJSON(path);
                if (err) {
                    onEnd(err, ...args);
                } else {
                    onEnd(null, ...args);
                }
            };

            if (_.isNil(err) === false) {
                if (err.code === resourceNotAvailable) {
                    return unLockCallback(ErrorCode.get503());
                } else if (err.code === noSuchFile) {
                    return unLockCallback(ErrorCode.get404());
                } else {
                    return unLockCallback(err);
                }
            }

            updateHandler(json, (err) => {
                if (err) {
                    return unLockCallback(err);
                } else {
                    onWrite(name, transformPackage(json), unLockCallback);
                }
            });
        });
    }

    deletePackage(fileName: string, callback: Callback) {
        return this.remoteFSImpl.deleteFile(this._getStorage(fileName), callback);
    }

    removePackage(callback: Callback): void {
        this.remoteFSImpl.deleteFile(this._getStorage('.'), callback);
    }

    _save(name: string, value: Package, cb: Function) {
        this.remoteFSImpl.writeFile(name, this._convertToString(value), cb);
    }

    saveDatabase(name: string, value: any, cb: Function) {
        this._save(name, value, cb);
    }

    createPackage(name: string, value: Package, cb: Function) {
        this._save(this._getStorage(pkgFileName), value, cb);
    }

    savePackage(name: string, value: Package, cb: Function) {
        this._save(this._getStorage(pkgFileName), value, cb);
    }

    _read(path: string, cb: Function) {
        this.remoteFSImpl.readFile(path, (err, data) => {
            if (err && err.code === 'NoSuchKey') {
                err.code = noSuchFile;
            }
            cb(err, data);
        });
    }

    readPackage(name: string, cb: Function) {
        this._read(this._getStorage(pkgFileName), cb);
    }

    readDatabase(path: string, cb: Function) {
        this._read(path, cb);
    }

    writeTarball(name: string): IUploadTarball {
        const uploadStream = new UploadTarball();

        let _ended = 0;
        uploadStream.on('end', () => {
            _ended = 1;
        });

        const temporalName: string = path.join(this.path, name);
        const file = this.remoteFSImpl.createWriteStream(temporalName);
        let opened = false;

        uploadStream.pipe(file);

        // $FlowFixMe
        uploadStream.done = () => {
            const onEnd = function onEnd() {
                file.on('close', () => {
                    uploadStream.emit('success');
                });
            };
            if (_ended) {
                onEnd();
            } else {
                uploadStream.on('end', onEnd);
            }
        };

        // $FlowFixMe
        uploadStream.abort = () => {
            if (opened) {
                opened = false;
            }
            file.end();
        };

        file.on('open', () => {
            opened = true;
            // re-emitting open because it's handled in storage.js
            uploadStream.emit('open');
        });

        file.on('error', (err) => {
            uploadStream.emit('error', err);
        });

        return uploadStream;
    }

    readTarball(name: string, readTarballStream: any, callback: Function = () => {
    }) {
        const pathName: string = this._getStorage(name);

        readTarballStream = new ReadTarball();

        this.remoteFSImpl.fileExists(pathName, (err, stats) => {
            if (_.isNil(err) === false) {
                err.status = err.statusCode;
                if (err.status === 404) {
                    err.code = 'ENOENT';
                }
                return readTarballStream.emit('error', err);
            }
            readTarballStream.emit('content-length', stats.ContentLength);
            readTarballStream.emit('open');

            const readStream = this.remoteFSImpl.createReadStream(pathName);

            readStream.on('error', (err) => {
                readTarballStream.emit('error', err);
                readStream.close();
            });

            readStream.pipe(readTarballStream);
        });

        setImmediate(callback);

        return readTarballStream;
    }

    _convertToString(value: Package): string {
        return JSON.stringify(value, null, '\t');
    }

    _getStorage(name: string = '') {
        const storagePath: string = path.join(this.path, name);
        return storagePath;
    }

    _lockAndReadJSON(path: string, callback: Function) {
        if (_.isNil(this.packageSemaphores[path])) {
            this.packageSemaphores[path] = new Semaphore(1);
        }

        this.packageSemaphores[path].take(1, () => {
            this._read(path, (readError, data) => {
                return callback(readError, data);
            });
        });
    }

    _unlockJSON(path: string) {
        if (this.packageSemaphores[path]) {
            this.packageSemaphores[path].leave(1);
        }
    }
}

export default RemoteFS;
