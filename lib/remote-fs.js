'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.noSuchFile = exports.fileExist = undefined;

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _semaphore = require('./semaphore');

var _semaphore2 = _interopRequireDefault(_semaphore);

var _s3FsImplementation = require('./s3-fs-implementation');

var _s3FsImplementation2 = _interopRequireDefault(_s3FsImplementation);

var _httpErrors = require('http-errors');

var _httpErrors2 = _interopRequireDefault(_httpErrors);

var _streams = require('@verdaccio/streams');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const fileExist = exports.fileExist = 'EEXISTS';
const noSuchFile = exports.noSuchFile = 'ENOENT';

const resourceNotAvailable = 'EAGAIN';
const pkgFileName = 'package.json';
const fSError = function (message, code = 409) {
    const err = (0, _httpErrors2.default)(code, message);
    // $FlowFixMe
    err.code = message;
    return err;
};

const ErrorCode = {
    get503: () => {
        return fSError('resource temporarily unavailable', 500);
    },
    get404: customMessage => {
        return fSError('no such package available', 404);
    }
};

class RemoteFS {

    constructor(path, bucketName, remoteFSOptions, logger) {
        this.path = path;
        this.logger = logger;
        this.remoteFSImpl = new _s3FsImplementation2.default(remoteFSOptions, logger);
        this.packageSemaphores = {};
    }

    /**
     *  This function allows to update the package thread-safely
     Algorithm:
     1. lock package.json for writing
     2. read package.json
     3. updateFn(pkg, cb), and wait for cb
     4. write package.json.tmp
     5. move package.json.tmp package.json
     6. callback(err?)
     * @param {*} name
     * @param {*} updateHandler
     * @param {*} onWrite
     * @param {*} transformPackage
     * @param {*} onEnd
     */
    updatePackage(name, updateHandler, onWrite, transformPackage, onEnd) {
        const path = this._getStorage(pkgFileName);
        this._lockAndReadJSON(path, (err, json) => {
            const self = this;
            // callback that cleans up lock first
            const unLockCallback = function unLockCallback(err, ...args) {
                self._unlockJSON(path);
                if (err) {
                    onEnd(err, ...args);
                } else {
                    onEnd(null, ...args);
                }
            };

            if (_lodash2.default.isNil(err) === false) {
                if (err.code === resourceNotAvailable) {
                    return unLockCallback(ErrorCode.get503());
                } else if (err.code === noSuchFile) {
                    return unLockCallback(ErrorCode.get404());
                } else {
                    return unLockCallback(err);
                }
            }

            updateHandler(json, err => {
                if (err) {
                    return unLockCallback(err);
                } else {
                    onWrite(name, transformPackage(json), unLockCallback);
                }
            });
        });
    }

    deletePackage(fileName, callback) {
        return this.remoteFSImpl.deleteFile(this._getStorage(fileName), callback);
    }

    removePackage(callback) {
        this.remoteFSImpl.deleteFile(this._getStorage('.'), callback);
    }

    _save(name, value, cb) {
        this.remoteFSImpl.writeFile(name, this._convertToString(value), cb);
    }

    saveDatabase(name, value, cb) {
        this._save(name, value, cb);
    }

    createPackage(name, value, cb) {
        this._save(this._getStorage(pkgFileName), value, cb);
    }

    savePackage(name, value, cb) {
        this._save(this._getStorage(pkgFileName), value, cb);
    }

    _read(path, cb) {
        this.remoteFSImpl.readFile(path, (err, data) => {
            if (err && err.code === 'NoSuchKey') {
                err.code = noSuchFile;
            }
            cb(err, data);
        });
    }

    readPackage(name, cb) {
        this._read(this._getStorage(pkgFileName), cb);
    }

    readDatabase(path, cb) {
        this._read(path, cb);
    }

    writeTarball(name) {
        const uploadStream = new _streams.UploadTarball();

        let _ended = 0;
        uploadStream.on('end', () => {
            _ended = 1;
        });

        const temporalName = _path2.default.join(this.path, name);
        const file = this.remoteFSImpl.createWriteStream(temporalName);
        let opened = false;

        uploadStream.pipe(file);

        // $FlowFixMe
        uploadStream.done = () => {
            const onEnd = function onEnd() {
                file.on('close', () => {
                    uploadStream.emit('success');
                });
            };
            if (_ended) {
                onEnd();
            } else {
                uploadStream.on('end', onEnd);
            }
        };

        // $FlowFixMe
        uploadStream.abort = () => {
            if (opened) {
                opened = false;
            }
            file.end();
        };

        file.on('open', () => {
            opened = true;
            // re-emitting open because it's handled in storage.js
            uploadStream.emit('open');
        });

        file.on('error', err => {
            uploadStream.emit('error', err);
        });

        return uploadStream;
    }

    readTarball(name, readTarballStream, callback = () => {}) {
        const pathName = this._getStorage(name);

        readTarballStream = new _streams.ReadTarball();

        this.remoteFSImpl.fileExists(pathName, (err, stats) => {
            if (_lodash2.default.isNil(err) === false) {
                err.status = err.statusCode;
                if (err.status === 404) {
                    err.code = 'ENOENT';
                }
                return readTarballStream.emit('error', err);
            }
            readTarballStream.emit('content-length', stats.ContentLength);
            readTarballStream.emit('open');

            const readStream = this.remoteFSImpl.createReadStream(pathName);

            readStream.on('error', err => {
                readTarballStream.emit('error', err);
                readStream.close();
            });

            readStream.pipe(readTarballStream);
        });

        setImmediate(callback);

        return readTarballStream;
    }

    _convertToString(value) {
        return JSON.stringify(value, null, '\t');
    }

    _getStorage(name = '') {
        const storagePath = _path2.default.join(this.path, name);
        return storagePath;
    }

    _lockAndReadJSON(path, callback) {
        if (_lodash2.default.isNil(this.packageSemaphores[path])) {
            this.packageSemaphores[path] = new _semaphore2.default(1);
        }

        this.packageSemaphores[path].take(1, () => {
            this._read(path, (readError, data) => {
                return callback(readError, data);
            });
        });
    }

    _unlockJSON(path) {
        if (this.packageSemaphores[path]) {
            this.packageSemaphores[path].leave(1);
        }
    }
}

exports.default = RemoteFS;