'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _s3WriteStream = require('./s3-write-stream');

var _s3WriteStream2 = _interopRequireDefault(_s3WriteStream);

var _awsSdk = require('aws-sdk');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

class S3FSImplementation {

    constructor(options, logger) {
        const params = {
            accessKeyId: options.accessKeyId,
            secretAccessKey: options.secretAccessKey,
            region: options.region || 'us-east-2'
        };
        this.s3 = new _awsSdk.S3(params);
        this.bucketName = options.bucket;
        this.logger = logger;
    }

    deleteFile(path, callback) {
        const delParams = {
            Bucket: this.bucketName,
            Key: path
        };
        this.s3.deleteObject(delParams, (err, delData) => {
            if (err) {
                callback(err);
            } else {
                callback(null, delData);
            }
        });
    }

    deleteDir(path, callback) {
        let finalPath = path;
        if (finalPath[finalPath.length - 1] !== '/') {
            finalPath = finalPath + '/';
        }
        this.deleteFile(finalPath, callback);
    }

    fileExists(path, callback) {
        const headParams = {
            Bucket: this.bucketName,
            Key: path
        };
        this.s3.headObject(headParams, (err, headData) => {
            if (err) {
                callback(err);
            } else {
                callback(null, headData);
            }
        });
    }

    renameFile(sourcePath, destinationPath, callback) {
        const copyParams = {
            Bucket: this.bucketName,
            CopySource: `/${this.bucketName}/${sourcePath}`,
            Key: destinationPath
        };
        this.s3.copyObject(copyParams, (copyErr, copyData) => {
            if (copyErr) {
                callback(copyErr);
            } else {
                const delParams = {
                    Bucket: this.bucketName,
                    Key: sourcePath
                };
                this.s3.deleteObject(delParams, (delErr, delData) => {
                    if (delErr) {
                        callback(delErr);
                    } else {
                        callback(null, copyData, delData);
                    }
                });
            }
        });
    }

    readFile(path, callback) {
        const params = {
            Bucket: this.bucketName,
            Key: path
        };
        this.s3.getObject(params, (err, res) => {
            if (err) {
                callback(err);
            } else {
                try {
                    const stringData = res.Body.toString('utf8'); // The response body is a byte array.
                    const data = JSON.parse(stringData);
                    return callback(null, data);
                } catch (err) {
                    return callback(err);
                }
            }
        });
    }

    createReadStream(path) {
        const params = {
            Bucket: this.bucketName,
            Key: path
        };
        return this.s3.getObject(params).createReadStream();
    }

    createWriteStream(path) {
        return new _s3WriteStream2.default(this.s3, this.bucketName, path, {});
    }

    writeFile(path, data, callback) {
        const params = {
            Bucket: this.bucketName,
            Key: path,
            Body: data
        };
        this.s3.putObject(params, (err, data) => {
            if (err) {
                callback(err);
            } else {
                callback(err, data);
            }
        });
    }

}

exports.default = S3FSImplementation;