'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});


class Semaphore {

    constructor(capacity) {
        capacity = capacity || 1;
        this.capacity = capacity;
        this.current = 0;
        this.queue = [];
        this.firstHere = false;
    }

    take(n, taskToRun) {
        let isFirst = 0;
        if (this.firstHere === false) {
            this.current++;
            this.firstHere = true;
            isFirst = 1;
        }

        n = n || 1;

        const item = {
            n: n,
            task: taskToRun
        };

        let task = item.task;
        item.task = () => {
            task(this.leave.bind(this));
        };

        if (this.current + item.n - isFirst > this.capacity) {
            if (isFirst === 1) {
                this.current--;
                this.firstHere = false;
            }
            return this.queue.push(item);
        }

        this.current += item.n - isFirst;
        item.task(this.leave.bind(this));
        if (isFirst === 1) {
            this.firstHere = false;
        }
    }

    leave(n) {
        n = n || 1;

        this.current -= n;

        if (!this.queue.length) {
            if (this.current < 0) {
                throw new Error('leave called too many times.');
            }
            return;
        }

        const item = this.queue[0];

        if (item.n + this.current > this.capacity) {
            return;
        }

        this.queue.shift();
        this.current += item.n;

        process.nextTick(item.task);
    }
}

exports.Semaphore = Semaphore;
exports.default = Semaphore;