'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

/*
 * The following code was adapted from the s3-fs by Riptide Software Inc.
 */

const Writable = require('stream').Writable;

const defaultOptions = {};
const maxParts = 1000;
const partBufferSize = 5242880;

class MultiPartManager {

    constructor(client, bucket, key, options) {
        this.client = client;
        this.bucket = bucket;
        this.key = key;
        this.parts = [];
        this.partNumber = 0;
        this.currentBuffer = Buffer.alloc(0);
        this.bytesWritten = 0;
        this.options = options || {};
        this._uploadIdPromise = null;
    }

    addChunk(chunk) {
        this.currentBuffer = Buffer.concat([this.currentBuffer, chunk]);
        if (this.currentBuffer.length >= partBufferSize) {
            const promise = this.addPart(this.currentBuffer);
            this.parts.push(promise);
            this.currentBuffer = new Buffer(0);
        }
    }

    flush() {
        if (this.currentBuffer.length) {
            const promise = this.addPart(this.currentBuffer);
            this.parts.push(promise);
            this.currentBuffer = new Buffer(0);
        }
    }

    addPart(buffer) {
        const partNumber = ++this.partNumber;
        let returnPromise;

        if (partNumber > maxParts) {
            const err = new Error(`Unable to create partNumber:${partNumber}. The max partNumber is ${maxParts}`);
            returnPromise = this.abort().then(() => {
                return Promise.reject(err);
            }).catch(reason => {
                const message = err.message;
                const mergedError = new Error(`Error aborting upload: ${reason.message}. Upload was aborted due to: ${message}`);
                return Promise.reject(mergedError);
            });
        } else {
            returnPromise = this.uploadId().then(uploadId => {
                return new Promise((resolve, reject) => {
                    const params = {
                        Bucket: this.bucket,
                        Key: this.key,
                        Body: buffer,
                        UploadId: uploadId,
                        PartNumber: partNumber
                    };

                    this.client.uploadPart(params, (err, result) => {
                        if (err) {
                            const message = err.message;
                            return this.abort().then(() => {
                                reject(err);
                            }).catch(reason => {
                                const mergedError = new Error(`Error aborting upload: ${reason.message}. Upload was aborted due to: ${message}`);
                                reject(mergedError);
                            });
                        }
                        result.PartNumber = partNumber;
                        this.bytesWritten += buffer.length;
                        resolve(result);
                    });
                });
            });
        }
        // Synchronously attach an empty error handler to suppress unhandled rejection warnings.
        // Error will be handled asynchronously on complete() call.
        returnPromise.catch(() => {});
        return returnPromise;
    }

    abort() {
        return this.uploadId().then(uploadId => {
            return new Promise((resolve, reject) => {
                const params = {
                    Bucket: this.bucket,
                    Key: this.key,
                    UploadId: uploadId
                };

                this.client.abortMultipartUpload(params, err => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve();
                    }
                });
            });
        });
    }

    uploadId() {
        if (!this._uploadIdPromise) {
            this._uploadIdPromise = new Promise((resolve, reject) => {
                const params = Object.assign({
                    Bucket: this.bucket,
                    Key: this.key
                }, this.options);

                this.client.createMultipartUpload(params, (err, data) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(data.UploadId);
                    }
                });
            });
            return this._uploadIdPromise;
        } else {
            return this._uploadIdPromise;
        }
    }

    put() {
        return new Promise((resolve, reject) => {
            const putParams = Object.assign({
                Bucket: this.bucket,
                Key: this.key,
                Body: this.currentBuffer
            }, this.options);

            this.client.putObject(putParams, (err, data) => {
                if (err) {
                    reject(err);
                } else {
                    this.bytesWritten += this.currentBuffer.length;
                    resolve(data);
                }
            });
        });
    }

    complete() {
        if (this.partNumber > 0) {
            return this.uploadId().then(uploadId => {
                this.flush();
                return Promise.all(this.parts).then(parts => {
                    return new Promise((resolve, reject) => {
                        const params = {
                            Bucket: this.bucket,
                            Key: this.key,
                            UploadId: uploadId,
                            MultipartUpload: {
                                Parts: parts
                            }
                        };
                        this.client.completeMultipartUpload(params, (err, data) => {
                            if (err) {
                                reject(err);
                            } else {
                                resolve(data);
                            }
                        });
                    });
                });
            });
        } else {
            // if we did not reach the part limit of 5M just use putObject
            return this.put();
        }
    }
}

class S3WriteStream extends Writable {

    constructor(client, bucket, key, options) {
        const streamOptions = Object.assign({}, defaultOptions, options);
        super(streamOptions);
        this.multiPartManager = new MultiPartManager(client, bucket, key, options);
        this.bytesWritten = 0;
        setTimeout(() => {
            this.emit('open');
        }, 1);
    }

    write(chunk, encoding, cb) {
        this.multiPartManager.addChunk(chunk);
        if (cb) {
            cb(null);
        }
    }

    end(chunk, encoding, cb) {
        if (chunk) {
            this.multiPartManager.addChunk(chunk);
        }
        this.multiPartManager.complete().then(() => {
            this.bytesWritten = this.multiPartManager.bytesWritten;
            this.emit('close');
            if (cb) {
                cb(null);
            }
        }).catch(reason => {
            this.bytesWritten = this.multiPartManager.bytesWritten;
            this.emit('error', reason);
            if (cb) {
                return cb(reason);
            }
        });
    }
}

exports.default = S3WriteStream;