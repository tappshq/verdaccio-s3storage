'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.RemoteDatabase = undefined;

var _remoteDatabase = require('./remote-database');

var _remoteDatabase2 = _interopRequireDefault(_remoteDatabase);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.RemoteDatabase = _remoteDatabase2.default;
exports.default = _remoteDatabase2.default;