'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _remoteFs = require('./remote-fs');

var _remoteFs2 = _interopRequireDefault(_remoteFs);

var _semaphore = require('./semaphore');

var _semaphore2 = _interopRequireDefault(_semaphore);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Handle local database.
 */
class RemoteDatabase {

    /**
     * Load an parse the local json database.
     */
    constructor(config, logger) {
        this.dbSem = new _semaphore2.default(1);
        const storeConf = config.store;
        this.s3options = storeConf.s3storage;
        this.bucketName = storeConf.s3storage.bucket;
        this.config = config;
        this.path = this._buildStoragePath(config);
        this.remotefs = new _remoteFs2.default(this.path, this.bucketName, this.s3options, logger);
        this.logger = logger.logger;
        this.locked = false;
        this._fetchLocalPackages();
    }

    getSecret() {
        return Promise.resolve(this.data.secret);
    }

    setSecret(secret) {
        return new Promise(resolve => {
            this.data.secret = secret;
            resolve(this._sync());
        });
    }

    /**
     * Add a new element.
     */
    add(name, callback) {
        if (this.data.list.indexOf(name) === -1) {
            this.data.list.push(name);
            callback(this._sync());
        }
    }

    /**
     * Synchronize the remote storage with the local one.
     * @private
     */
    _sync() {
        if (this.locked) {
            this.logger.info('Database is locked, please check error message printed during startup to prevent data loss.');
            return new Error('Verdaccio database is locked, please contact your administrator to checkout logs during verdaccio startup.');
        }

        this.dbSem.take(1, () => {
            this.remotefs.saveDatabase(this.path, this.data, err => {
                this.dbSem.leave(1);
                return err;
            });
        });
    }

    /**
     * Remove an element from the database.
     */
    remove(name, callback) {
        const pkgName = this.data.list.indexOf(name);
        if (pkgName !== -1) {
            this.data.list.splice(pkgName, 1);
            // Should we have the ability to delete packages?
            this.remotefs.deletePackage(name, callback);
        }
        callback(this._sync());
    }

    /**
     * Return all database elements.
     */
    get(callback) {
        callback(null, this.data.list);
    }

    getPackageStorage(packageInfo) {
        // $FlowFixMe
        const packagePath = this._getLocalStoragePath(this.config.getMatchedPackagesSpec(packageInfo).storage);

        if (_lodash2.default.isString(packagePath) === false) {
            this.logger.debug({ name: packageInfo }, 'this package has no storage defined: @{name}');
            return;
        }

        const packageStoragePath = _path2.default.join(packagePath, packageInfo);

        return new _remoteFs2.default(packageStoragePath, this.bucketName, this.s3options, this.logger);
    }

    search(onPackage, onEnd, validateName) {
        // TODO: Implement search
        onEnd();
    }

    /**
     * Verify the right local storage location.
     * @private
     */
    _getLocalStoragePath(path) {
        if (_lodash2.default.isNil(path) === false) {
            return path;
        }

        return this.config.storage;
    }

    /**
     * Build the local database path.
     * @private
     */
    _buildStoragePath(config) {
        // FUTURE: the database might be parameterizable from config.yaml
        return _path2.default.join(config.storage, '.sinopia-db.json');
    }

    /**
     * Fetch local packages.
     * @private
     */
    _fetchLocalPackages() {
        const database = [];
        this.data = { list: database, secret: '' };

        this.remotefs.readDatabase(this.path, (err, dbData) => {
            if (err) {
                this.logger.error('Failed to read package database file, please check the error printed below:\n' + `File Path: ${this.path}\n\n ${err.message}`, err.stack);
                this.locked = true;
            } else {
                this.data = dbData;
                this.data.secret = this.config.checkSecretKey(this.data.secret);
            }
        });
    }
}

exports.default = RemoteDatabase;