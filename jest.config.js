/* eslint comma-dangle: 0 */

module.exports = {
  name: 's3-storage-jest',
  verbose: true,
  collectCoverage: true,
  coveragePathIgnorePatterns: ['node_modules', '_storage', 'fixtures', 'lib']
};
